// Теоретичні питання
// 1.Опишіть своїми словами, що таке метод об'єкту
//Це функція в середені об'єкту, що є такоє і його властивістю. Додає додаткой функціонал, інтерактив до сторінки.

// 2.Який тип даних може мати значення властивості об'єкта?
//Стринг, намбер, буалиан, нал, андефайнд, симбол.

// 3.Об'єкт це посилальний тип даних. Що означає це поняття?
//Це означає, що він об'єкт сам не копіюється, а копіюється посілання на його місце, де він зберігається(на комірку пам'яті).

// Завдання
// Реалізувати функцію створення об'єкта "юзер". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
// При виклику функція повинна запитати ім'я та прізвище.
// Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
// Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
// Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). Вивести у консоль результат виконання функції.
// Необов'язкове завдання підвищеної складності
// Зробити так, щоб властивості firstName та lastName не можна було змінювати напряму. Створити функції-сеттери setFirstName() та setLastName(), які дозволять змінити дані властивості.


function createNewUser(){
    this.firstName = prompt('What is your firstname');
    if (this.firstName.indexOf(' ') > -1 || !isNaN(this.firstName)){
            do {
                alert('What is your firstname');
                this.firstName = prompt('What is your firstname');
              } while  (this.firstName.indexOf(' ') > -1 || !isNaN(this.firstName))
        }

    this.lastName = prompt('What is your lastname');
    if (this.lastName.indexOf(' ') > -1 || !isNaN(this.lastName)){
        do {
            alert('What is your lastname');
            this.lastName = prompt('What is your lastname');
          } while  (this.lastName.indexOf(' ') > -1 || !isNaN(this.lastName))
    }
    this.getLogin = function() {
        let userLogin = this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        console.log(userLogin);
        return userLogin;
   
    }
}

let newUser = new createNewUser();
alert(`Your login is: ${newUser.getLogin()}`);
console.log(newUser);


